from collections.abc import Sequence, Mapping


_INT_START = b'i'
_LIST_START = b'l'
_DICT_START = b'd'
_STR_SEP = b':'
_END = b'e'


def encode(data):
    for data_type, type_encoder in _ENCODERS.items():
        if isinstance(data, data_type):
            return type_encoder(data)
    raise TypeError(f'Cannot encode "{type(data)}".')


def _encode_int(data):
    return _INT_START + str.encode(str(data)) + _END


def _encode_bytes(data):
    length = str.encode(str(len(data)))
    return length + _STR_SEP + data


def _encode_str(data):
    return _encode_bytes(str.encode(data))


def _encode_list(data):
    result = _LIST_START
    for item in data:
        result += encode(item)
    result += _END
    return result


def _encode_dict(data):
    result = _DICT_START
    for item_key, item_val in sorted(data.items()):
        if not isinstance(item_key, (bytes, str)):
            raise TypeError(f'Wrong dict key type {type(item_key)}.')
        key = encode(item_key)
        value = encode(item_val)
        result += (key + value)
    result += _END
    return result


_ENCODERS = {
    int: _encode_int,
    str: _encode_str,
    bytes: _encode_bytes,
    Sequence: _encode_list,
    Mapping: _encode_dict
}
