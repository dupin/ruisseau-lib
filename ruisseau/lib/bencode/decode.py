def decode(data):
    if not data:
        raise RuntimeError('No data.')
    if not isinstance(data, bytes):
        raise TypeError('Argument "data" must be of type bytes.')
    return _decode(_Reader(data))


_INT_START = b'i'
_LIST_START = b'l'
_DICT_START = b'd'
_STR_START = [bytes([i]) for i in b'0123456789']
_STR_SEP = b':'
_END = b'e'


def _decode(reader):
    token = reader.peek()
    try:
        decoder = _DECODERS[token]
    except KeyError as exc:
        raise RuntimeError(
            f'{reader.index}: Unknown token "{reader.peek().decode()}".'
        ) from exc
    return decoder(reader)


def _decode_int(reader):
    reader.accept(_INT_START)
    return int(reader.read_until(_END))


def _decode_bytes(reader):
    length = int(reader.read_until(_STR_SEP))
    return reader.read(length)


def _decode_list(reader):
    res = []
    reader.accept(_LIST_START)
    while reader.peek() != _END:
        res.append(_decode(reader))
    reader.accept(_END)
    return res


def _decode_dict(reader):
    dic = {}
    reader.accept(_DICT_START)
    while reader.peek() != _END:
        key = _decode_bytes(reader)
        value = _decode(reader)
        dic[key] = value
    reader.accept(_END)
    return dic


_DECODERS = {
    _INT_START: _decode_int,
    _LIST_START: _decode_list,
    _DICT_START: _decode_dict
    }
_DECODERS.update({token: _decode_bytes for token in _STR_START})


class _Reader:
    """Low level bytes reader utility."""

    def __init__(self, data):
        self._data = data
        self._index = 0

    @property
    def index(self):
        return self._index

    def peek(self, length=1):
        if self._index + length > len(self._data):
            raise RuntimeError(f'{self._index}: Cannot read {length} bytes.')
        return self._data[self._index:self._index+length]

    def accept(self, token):
        read_token = self.read(1)
        assert read_token == token

    def read(self, length):
        res = self.peek(length)
        self._index += length
        return res

    def read_until(self, token):
        try:
            pos = self._data.index(token, self._index)
            res = self._data[self._index:pos]
            self._index = pos + len(token)
            return res
        except ValueError as exc:
            raise RuntimeError(
                f'{self._index}: Could not find token "{token.decode()}".'
            ) from exc
