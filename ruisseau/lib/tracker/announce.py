import asyncio
import math
import time


class Announcer:
    """Announcer state machine."""

    INTERVAL_MIN = 60
    INTERVAL_MAX = 3600

    def __init__(self, adapter, torrent_infos):
        self.status = 'stopped'
        self._adapter = adapter
        self._interval = _Interval()
        self._request_factory = _RequestFactory(torrent_infos)
        self._announce_mutex = asyncio.Lock()

    @property
    def time_to_announce(self):
        return self._interval.time_ahead()

    async def announce(self):
        request = self._request_factory.make_regular(self.status)
        peers = await self._do_announce(request)
        self._update_status(request['event'])
        return peers

    async def stop(self):
        request = self._request_factory.make_stopped()
        peers = await self._do_announce(request)
        self.status = 'stopped'
        return peers

    async def _do_announce(self, request):
        async with self._announce_mutex:
            try:
                response = await self._adapter.announce(request)
            except Exception as exception:
                self._interval.tick()
                self._interval.set(self.INTERVAL_MAX)
                raise exception
        self._interval.tick()
        self._process_response(response)
        return response.peers

    def _update_status(self, request_event):
        if request_event is not None:
            self.status = request_event

    def _process_response(self, response):
        try:
            interval = int(response.interval)
        except (AttributeError, ValueError) as origin:
            self._interval.set(self.INTERVAL_MAX)
            raise ResponseError(response) from origin
        self._interval.set(min(max(
            interval, self.INTERVAL_MIN), self.INTERVAL_MAX))


class ResponseError(Exception):
    """Raised when response is invalid."""

    def __init__(self, response):
        super().__init__()
        self.response = response


class _RequestFactory:

    def __init__(self, torrent_infos):
        self._infos = torrent_infos

    def make_regular(self, current_status):
        event = self._build_event(current_status)
        return self._build(event)

    def make_stopped(self):
        return self._build('stopped')

    def _build_event(self, current_status):
        if current_status != 'completed' \
                and self._infos['progress']['left'] == 0:
            event = 'completed'
        elif current_status == 'stopped':
            event = 'started'
        else:
            event = None
        return event

    def _build(self, event):
        return {'info_hash': self._infos['info_hash'],
                'peer_id': self._infos['peer_id'],
                'port': self._infos['port'],
                'uploaded': self._infos['progress']['uploaded'],
                'downloaded': self._infos['progress']['downloaded'],
                'left': self._infos['progress']['left'],
                'event': event}


class _Interval:
    """Calculate "time ahead" given duration and tick."""

    def __init__(self):
        self._duration = 0
        self._last_tick = None

    def set(self, duration):
        assert duration >= 0, 'duration must be positive'
        self._duration = duration

    def tick(self):
        self._last_tick = time.time()

    def time_ahead(self):
        return self.interval_time_ahead(self._duration, self._last_tick)

    @staticmethod
    def interval_time_ahead(interval, last_tick):
        assert interval >= 0, 'interval must be positive'
        if last_tick is None:
            return 0
        now = time.time()
        assert last_tick < now, 'last_tick must be in the past'
        return math.ceil(interval - (now-last_tick))
