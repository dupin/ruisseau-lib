from socket import inet_ntoa
from struct import unpack
from urllib.parse import urlencode

import aiohttp

from ruisseau.lib.bencode import decode


class HttpAdapter:

    SCHEMES = ('http', 'https')

    def __init__(self, url):
        self._url_factory = _UrlFactory(url)

    async def announce(self, request):
        url = self._url_factory.from_request(request)
        response = await self._fetch(url)
        return _ResponseFactory().from_bencoded(response)

    @staticmethod
    async def _fetch(url):
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as response:
                if response.status != 200:
                    raise AnnounceError(f'HTTP {response.status}')
                return await response.read()


class AnnounceError(Exception):
    """Raised when error occur during announce."""


class _UrlFactory:

    PARAMS = {'info_hash', 'peer_id', 'port', 'uploaded',
              'downloaded', 'left', 'event'}

    def __init__(self, base_url, compact=True):
        self.base_url = base_url
        self.compact = compact

    def from_request(self, request):
        query_string = urlencode(self.make_params(request))
        return f'{self.base_url}?{query_string}'

    def make_params(self, request):
        params = {k: v for k, v in request.items() if k in self.PARAMS}
        params['compact'] = 1 if self.compact else 0
        return params


class _ResponseFactory:

    def from_bencoded(self, bencoded):
        response = decode(bencoded)
        return self._parse(response)

    def _parse(self, response):
        if b'failure reason' in response:
            raise AnnounceError(response[b'failure reason'].decode('utf-8'))
        interval = response[b'interval']
        peers = self._parse_peers(response[b'peers'])
        return {'peers': peers, 'interval': interval}

    def _parse_peers(self, peers):
        if isinstance(peers, bytes):
            return self._parse_binary_peers(peers)
        if isinstance(peers, list):
            return self._parse_dict_peers(peers)
        raise ValueError()

    @staticmethod
    def _parse_dict_peers(peers):
        return [{'host': p[b'ip'].decode('utf-8'),
                 'port': p[b'port'],
                 'id': p[b'peer id'].decode('utf-8')}
                for p in peers]

    def _parse_binary_peers(self, peers):
        peers_tuple = self._decode_binary_peer(peers)
        return [{'host': host, 'port': port} for (host, port) in peers_tuple]

    @staticmethod
    def _decode_binary_peer(peers):
        ip_len = 4
        port_len = 2
        peer_len = ip_len + port_len
        if (len(peers) % peer_len) != 0:
            raise ValueError(f'Binary peers not multiple of {peer_len} bytes')
        offsets = range(0, len(peers), peer_len)
        chunks = (peers[i:i+peer_len] for i in offsets)
        return ((inet_ntoa(c[:ip_len]), _decode_int(c[ip_len:]))
                for c in chunks)


def _decode_int(packed):
    return unpack(">H", packed)[0]
