from collections import ChainMap
import importlib
import itertools
import pkgutil

import ruisseau.lib.tracker.adapters


class AdapterFactory:
    """Look in the ruisseau.lib.tracker.adapters namespace and search for a
    class that provide SCHEMES attribute and announce method.

    The SCHEMES attribute is a list of uri scheme that the adapter support.
    """

    def __init__(self, namespace=ruisseau.lib.tracker.adapters):
        self._namespace = namespace
        self._load_adapters()

    def from_uri(self, uri):
        src_scheme = uri.split("://")[0]
        return self._adapters[src_scheme]

    def _load_adapters(self):
        modules_info = self._iter_namespace(self._namespace)
        modules = (importlib.import_module(mi.name) for mi in modules_info)
        adapters = itertools.chain.from_iterable(
            (self._iter_adapter(m) for m in modules))
        self._adapters = ChainMap(
            *(self._index_scheme(adapter) for adapter in adapters))

    def _iter_adapter(self, module):
        for name in dir(module):
            obj = getattr(module, name)
            if self._is_adapter(obj):
                yield obj

    @staticmethod
    def _index_scheme(adapter):
        return {scheme: adapter for scheme in adapter.SCHEMES}

    @staticmethod
    def _iter_namespace(ns_pkg):
        return pkgutil.iter_modules(ns_pkg.__path__, ns_pkg.__name__ + ".")

    @staticmethod
    def _is_adapter(obj):
        return hasattr(obj, 'SCHEMES') and hasattr(obj, 'announce')
