import hashlib
import os

from ruisseau.lib import bencode


def from_bencoded(metainfo):
    decoded = bencode.decode(metainfo)
    infohash = _gen_infohash(decoded[b'info'])
    return from_dict(decoded, infohash)


def from_dict(metainfo, infohash):
    return {
        'announce': _decode(metainfo[b'announce']),
        'infohash': infohash,
        'files': _files(metainfo[b'info']),
        'pieces': _pieces(metainfo[b'info'])
        }


def _gen_infohash(info_dict):
    return hashlib.sha1(bencode.encode(info_dict)).digest()


def _decode(bytes_string):
    return bytes_string.decode('utf_8')


def _files(info):
    name = _decode(info[b'name'])
    _check_file_name(name)
    try:
        result = [{'path': name, 'size': info[b'length']}]
    except KeyError:
        result = [_map_file_entry(name, entry) for entry in info[b'files']]
    if not result:
        raise RuntimeError('metainfo: empty files entry')
    return result


def _map_file_entry(prefix, entry):
    path_parts = [_decode(p) for p in entry[b'path']]
    for part in path_parts:
        _check_file_name(part)
    return {'path': [prefix] + path_parts, 'size': entry[b'length']}


def _check_file_name(name):
    if os.sep in name:
        raise RuntimeError(f'metainfo: invalid file name "{name}"')


def _pieces(info):
    hlen = 20
    pieces_hash = info[b'pieces']
    pieces_count = len(pieces_hash)//hlen
    hash_list = [pieces_hash[i*hlen:(i*hlen)+hlen]
                 for i in range(pieces_count)]
    return {'size': info[b'piece length'], 'hash': hash_list}
