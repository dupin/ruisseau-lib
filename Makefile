SRCDIR=ruisseau

.PHONY: all lint flake8 pylint mypy test unittest mutmut coverage clean mrproper

all: lint test

lint: flake8 pylint mypy

flake8:
	flake8 ${SRCDIR}/*

pylint:
	-pylint -d R0903,C0111 ${SRCDIR}/*

mypy:
	mypy ${SRCDIR}

test: unittest mutmut

unittest:
	coverage run --branch --source=${SRCDIR} -m unittest

mutmut:
	mutmut run

coverage: unittest
	coverage report --show-missing --skip-empty

mrproper:
	find ./ -name '__pycache__' -exec rm -rf {} +
	rm .coverage
