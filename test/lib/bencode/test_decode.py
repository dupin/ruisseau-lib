import unittest
from ruisseau.lib.bencode import decode


class InvalidTestCase(unittest.TestCase):
    
    def test_string_input(self):
        with self.assertRaises(TypeError) as cm:
            decode('String input')
        self.assertEqual('Argument "data" must be of type bytes.', cm.exception.args[0])

    def test_empty(self):
        with self.assertRaises(RuntimeError) as cm:
            decode(b'')
        self.assertEqual('No data.', cm.exception.args[0])
    
    def test_unknown_token(self):
        with self.assertRaises(RuntimeError) as cm:
            decode(b'X:')
        self.assertEqual('0: Unknown token "X".', cm.exception.args[0])


class IntegerTestCase(unittest.TestCase):
    """Integers are represented by an 'i' followed by the number in base 10 followed by an 'e'. For example i3e
    corresponds to 3 and i-3e corresponds to -3. Integers have no size limitation. i-0e is invalid. All encodings
    with a leading zero, such as i03e, are invalid, other than i0e, which of course corresponds to 0."""

    def test_positive(self):
        self.assertEqual(1337, decode(b'i1337e'))

    def test_negative(self):
        self.assertEqual(-1337, decode(b'i-1337e'))

    def test_zero(self):
        self.assertEqual(0, decode(b'i0e'))

    def test_no_ending(self):
        with self.assertRaises(RuntimeError) as cm:
            decode(b'i1337')
        self.assertEqual('1: Could not find token "e".', cm.exception.args[0])

    def test_malformed(self):
        self.assertRaises(ValueError, decode, b'istringe')


class IntegerSideEffectTestCase(unittest.TestCase):
    """The use of builtin 'int()' function make these formats valid."""

    def test_positive_with_sign(self):
        self.assertEqual(1337, decode(b'i+1337e'))

    def test_whitespace_surrounded(self):
        self.assertEqual(1337, decode(b'i 1337 e'))

    def test_underscore_separator(self):
        self.assertEqual(1337, decode(b'i1_337e'))

    def test_zero_prefixed(self):
        self.assertEqual(1337, decode(b'i01337e'))


class StringTestCase(unittest.TestCase):
    """Strings are length-prefixed base ten followed by a colon and the string. For example 4:spam corresponds to
    'spam'."""

    def test_empty(self):
        self.assertEqual(b'', decode(b'0:'))

    def test_regular(self):
        self.assertEqual(b'hello', decode(b'5:hello'))

    def test_invalid_length(self):
        with self.assertRaises(RuntimeError) as cm:
            decode(b'30:hello')
        self.assertEqual('3: Cannot read 30 bytes.', cm.exception.args[0])


class ListTestCase(unittest.TestCase):
    """Lists are encoded as an 'l' followed by their elements (also bencoded) followed by an 'e'. For example
    l4:spam4:eggse corresponds to ['spam', 'eggs']."""

    def test_empty(self):
        self.assertEqual([], decode(b'le'))

    def test_integer(self):
        self.assertEqual([1, 2, 3], decode(b'li1ei2ei3ee'))

    def test_string(self):
        self.assertEqual([b'toto', b'hello', b'jah'], decode(b'l4:toto5:hello3:jahe'))

    def test_nested_list(self):
        self.assertEqual([[1, 2], [3, 4]], decode(b'lli1ei2eeli3ei4eee'))

    def test_dict(self):
        self.assertEqual([{b'abc': 0}, {b'def': 0}], decode(b'ld3:abci0eed3:defi0eee'))

    def test_various(self):
        self.assertEqual([b'toto', 1337, [1, 2, 3], {b'abc': 0}], decode(b'l4:totoi1337eli1ei2ei3eed3:abci0eee'))

    def test_no_ending(self):
        self.assertRaises(RuntimeError, decode, b'li1ei2ei3e')


class DictTestCase(unittest.TestCase):
    """Dictionaries are encoded as a 'd' followed by a list of alternating keys and their corresponding values
    followed by an 'e'. For example, d3:cow3:moo4:spam4:eggse corresponds to {'cow': 'moo', 'spam': 'eggs'} and
    d4:spaml1:a1:bee corresponds to {'spam': ['a', 'b']}. Keys must be strings and appear in sorted order (sorted as
    raw strings, not alphanumerics)."""

    def test_empy(self):
        self.assertEqual({}, decode(b'de'))

    def test_sorted(self):
        expected = {b'hello': 1337, b'toto': b'jah'}
        self.assertEqual(expected, decode(b'd5:helloi1337e4:toto3:jahe'))

    def test_unsorted(self):
        # This is not following the rules
        expected = {b'abc': 0, b'def': 0}
        self.assertEqual(expected, decode(b'd3:defi0e3:abci0ee'))

    def test_unsorted_2(self):
        # This is not following the rules
        expected = {b'publisher': b'bob', b'publisher-webpage': b'www.example.com', b'publisher.location': b'home'}
        encoded = b'd18:publisher.location4:home17:publisher-webpage15:www.example.com9:publisher3:bobe'
        self.assertEqual(expected, decode(encoded))

    def test_with_int_key(self):
        self.assertRaises(ValueError, decode, b'di123e4:totoe')

    def test_with_list_key(self):
        self.assertRaises(ValueError, decode, b'dl1:ae4:totoe')

    def test_with_dict_key(self):
        self.assertRaises(ValueError, decode, b'dde4:totoe')

    def test_no_ending(self):
        self.assertRaises(RuntimeError, decode, b'd5:helloi1337e4:toto3:jah')

    def test_no_value(self):
        self.assertRaises(RuntimeError, decode, b'd5:helloe')
