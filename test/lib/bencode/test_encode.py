"""BCode encoding test."""

import unittest
from ruisseau.lib.bencode import encode


class UnsuportedTestCase(unittest.TestCase):

    def test_float(self):
        with self.assertRaises(TypeError) as cm:
            encode(3.14)
        self.assertEqual('Cannot encode "<class \'float\'>".', cm.exception.args[0])

    def test_none(self):
        with self.assertRaises(TypeError) as cm:
            encode(None)
        self.assertEqual('Cannot encode "<class \'NoneType\'>".', cm.exception.args[0])


class IntegerTestCase(unittest.TestCase):
    """Integers are represented by an 'i' followed by the number in base 10 followed by an 'e'. For example i3e corresponds to 3 and i-3e corresponds to -3. Integers have no size limitation. i-0e is invalid. All encodings with a leading zero, such as i03e, are invalid, other than i0e, which of course corresponds to 0."""

    def test_positive(self):
        self.assertEqual(b'i1337e', encode(1337))

    def test_negative(self):
        self.assertEqual(b'i-12345e', encode(-12345))

    def test_zero(self):
        self.assertEqual(b'i0e', encode(0))


class StringTestCase(unittest.TestCase):
    """Strings are length-prefixed base ten followed by a colon and the string. For example 4:spam corresponds to 'spam'."""

    def test_empty(self):
        self.assertEqual(b'0:', encode(''))

    def test_regular(self):
        self.assertEqual(b'5:hello', encode('hello'))

    def test_unicode(self):
        self.assertEqual(b'2:\xc3\xa9', encode('é'))


class BinaryTestCase(unittest.TestCase):

    def test_empty(self):
        self.assertEqual(b'0:', encode(b''))

    def test_regular(self):
        self.assertEqual(b'5:hello', encode(b'hello'))


class ListTestCase(unittest.TestCase):
    """Lists are encoded as an 'l' followed by their elements (also bencoded) followed by an 'e'. For example l4:spam4:eggse corresponds to ['spam', 'eggs']."""

    def test_empty(self):
        self.assertEqual(b'le', encode([]))

    def test_integer(self):
        self.assertEqual(b'li1ei2ei3ee', encode([1, 2, 3]))

    def test_string(self):
        self.assertEqual(b'l4:toto5:hello3:jahe', encode(['toto', 'hello', 'jah']))

    def test_bytes(self):
        self.assertEqual(b'l4:toto5:hello3:jahe', encode([b'toto', b'hello', b'jah']))

    def test_nested_list(self):
        self.assertEqual(b'lli1ei2eeli3ei4eee', encode([[1, 2], [3, 4]]))

    def test_dict(self):
        self.assertEqual(b'ld3:abci0eed3:defi0eee', encode([{'abc': 0}, {'def': 0}]))

    def test_various(self):
        self.assertEqual(b'l4:totoi1337eli1ei2ei3eed3:abci0eee', encode(['toto', 1337, [1, 2, 3], {'abc': 0}]))

    def test_with_none(self):
        self.assertRaises(TypeError, encode, [None])


class DictTestCase(unittest.TestCase):
    """Dictionaries are encoded as a 'd' followed by a list of alternating keys and their corresponding values followed by an 'e'. For example, d3:cow3:moo4:spam4:eggse corresponds to {'cow': 'moo', 'spam': 'eggs'} and d4:spaml1:a1:bee corresponds to {'spam': ['a', 'b']}. Keys must be strings and appear in sorted order (sorted as raw strings, not alphanumerics)."""

    def test_empty(self):
        self.assertEqual(b'de', encode({}))

    def test_sorted(self):
        d = {'hello': 1337, 'toto': 'jah'}
        self.assertEqual(b'd5:helloi1337e4:toto3:jahe', encode(d))
    
    def test_unsorted(self):
        d = {'def': 0, 'abc': 0}
        self.assertEqual(b'd3:abci0e3:defi0ee', encode(d))

    def test_unsorted_2(self):
        d = {'publisher.location': 'home', 'publisher-webpage': 'www.example.com', 'publisher': 'bob'}
        encoded = b'd9:publisher3:bob17:publisher-webpage15:www.example.com18:publisher.location4:homee'
        self.assertEqual(encoded, encode(d))

    def test_raw(self):
        d = {'abc': 0, 'def': 0}
        self.assertEqual(b'd3:abci0e3:defi0ee', encode(d))

    def test_with_bytes_key(self):
        # This not following the rules ?
        d = {b'abc': 0, b'def': 0}
        self.assertEqual(b'd3:abci0e3:defi0ee', encode(d))

    def test_with_int_key(self):
        with self.assertRaises(TypeError) as cm:
            encode({0: 1})
        self.assertEqual('Wrong dict key type <class \'int\'>.', cm.exception.args[0])

    def test_with_none_key(self):
        with self.assertRaises(TypeError) as cm:
            encode({None: 1})
        self.assertEqual('Wrong dict key type <class \'NoneType\'>.', cm.exception.args[0])

    def test_with_none_value(self):
        self.assertRaises(TypeError, encode, {'test': None})
