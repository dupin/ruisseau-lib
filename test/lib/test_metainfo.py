import os
import unittest

from ruisseau.lib.metainfo import from_dict, from_bencoded


class SingleFileTestCase(unittest.TestCase):

    def setUp(self):
        pieces = b'1011121314151617181920212223242526272829'
        info = {b'name': b'thename', b'piece length': 512, b'pieces': pieces, b'length': 1000}
        self.metainfo = {b'announce': b'http://example.test', b'info': info}

    def test_single_file(self):
        actual = from_dict(self.metainfo, b'infohash')
        expected_files = [{'path': 'thename', 'size': 1000}]
        expected_pieces = {'size': 512, 'hash': [b'10111213141516171819', b'20212223242526272829']}
        expected = {'announce': 'http://example.test', 'infohash': b'infohash', 'pieces': expected_pieces, 'files': expected_files}
        self.assertEqual(actual, expected)

    def test_single_file_with_dir_name(self):
        name = os.sep + 'root'
        self.metainfo[b'info'][b'name'] = bytes(name, 'utf-8')
        with self.assertRaises(RuntimeError) as cm:
            from_dict(self.metainfo, b'infohash')
        self.assertEqual(cm.exception.args[0], f'metainfo: invalid file name "{name}"')


class MultiFileTestCase(unittest.TestCase):

    def setUp(self):
        pieces = b'1011121314151617181920212223242526272829'
        files = [{b'length': 400, b'path': [b'd1', b'f1']}, {b'length': 600, b'path': [b'f2']}]
        info = {b'name': b'thename', b'piece length': 512, b'pieces': pieces, b'files': files}
        self.metainfo = {b'announce': b'http://example.test', b'info': info}

    def test_multi_files(self):
        actual = from_dict(self.metainfo, b'infohash')
        expected_files = [{'path': ['thename', 'd1', 'f1'], 'size': 400}, {'path': ['thename', 'f2'], 'size': 600}]
        expected_pieces = {'size': 512, 'hash': [b'10111213141516171819', b'20212223242526272829']}
        expected = {'announce': 'http://example.test', 'infohash': b'infohash', 'pieces': expected_pieces, 'files': expected_files}
        self.assertEqual(actual, expected)

    def test_multi_file_without_file(self):
        self.metainfo[b'info'][b'files'] = []
        with self.assertRaises(RuntimeError) as cm:
            from_dict(self.metainfo, b'infohash')
        self.assertEqual(cm.exception.args[0], 'metainfo: empty files entry')

    def test_multi_files_with_dir(self):
        name = 'f1' + os.sep + 'f1'
        self.metainfo[b'info'][b'files'] = [{b'length': 400, b'path': [b'd1', bytes(name, 'utf-8')]}, {b'length': 600, b'path': [b'f2']}]
        with self.assertRaises(RuntimeError) as cm:
            from_dict(self.metainfo, b'infohash')
        self.assertEqual(cm.exception.args[0], f'metainfo: invalid file name "{name}"')


class BencodedTestCase(unittest.TestCase):

    def from_dict(self, metainfo, infohash):
        self.metainfo = metainfo
        self.infohash = infohash
        return self.origin_from_dict(metainfo, infohash)

    def test_bencoded(self):
        import ruisseau.lib.metainfo
        self.origin_from_dict = ruisseau.lib.metainfo.from_dict
        ruisseau.lib.metainfo.from_dict = self.from_dict
        bencoded = b'd8:announce19:http://example.test4:infod5:filesld6:lengthi400e4:pathl2:d12:f1eed6:lengthi600e4:pathl2:f2eee4:name7:thename12:piece lengthi512e6:pieces40:1011121314151617181920212223242526272829ee'
        torrent_info = from_bencoded(bencoded)
        expected_infohash = b'\x1a\x9c\x8e\x05e\xc6u6/\n\x94\xed9\xc9\xaf\xd0B\x91jK'
        expected_dict = {b'announce': b'http://example.test', b'info': {b'name': b'thename', b'piece length': 512, b'pieces': b'1011121314151617181920212223242526272829', b'files': [{b'length': 400, b'path': [b'd1', b'f1']}, {b'length': 600, b'path': [b'f2']}]}}
        self.assertEqual(expected_dict, self.metainfo)
        self.assertEqual(expected_infohash, self.infohash)
        self.assertEqual(torrent_info['infohash'], self.infohash)
