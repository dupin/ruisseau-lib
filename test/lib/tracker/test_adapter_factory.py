import unittest

from ruisseau.lib.tracker.adapter_factory import AdapterFactory
import test.lib.tracker


class InvalidAdapter:

    async def announce(self, request):
        pass


class FactoryTestCase(unittest.TestCase):

    def test_invalid_adapter(self):
        factory = AdapterFactory(test.lib.tracker)
        self.assertNotIn(InvalidAdapter, factory._adapters)
