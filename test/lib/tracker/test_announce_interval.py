import unittest
from time import time
from ruisseau.lib.tracker.announce import _Interval as Interval


class IntervalTimeAheadTest(unittest.TestCase):

    def test_advance(self):
        interval = 10
        last_call = time() - 8
        time_ahead = Interval.interval_time_ahead(interval, last_call)
        self.assertEqual(time_ahead, 2)

    def test_lateness(self):
        interval = 10
        last_call = time() - 13
        time_ahead = Interval.interval_time_ahead(interval, last_call)
        self.assertEqual(time_ahead, -3)

    def test_at_time(self):
        interval = 10
        last_call = time() - 10
        time_ahead = Interval.interval_time_ahead(interval, last_call)
        self.assertEqual(time_ahead, 0)

    def test_negative_interval(self):
        interval = -10
        last_call = time() - 3
        with self.assertRaises(AssertionError) as cm:
            Interval.interval_time_ahead(interval, last_call)
        self.assertEqual(cm.exception.args[0], 'interval must be positive')

    def test_future(self):
        interval = 10
        last_call = time() + 3
        with self.assertRaises(AssertionError) as cm:
            Interval.interval_time_ahead(interval, last_call)
        self.assertEqual(cm.exception.args[0], 'last_tick must be in the past')

    def test_now(self):
        interval = 10
        last_call = time()
        time_ahead = Interval.interval_time_ahead(interval, last_call)
        self.assertEqual(time_ahead, 10)

    def test_zero_interval(self):
        interval = 0
        last_call = time() - 3
        time_ahead = Interval.interval_time_ahead(interval, last_call)
        self.assertEqual(time_ahead, -3)

    def test_zero_interval_at_time(self):
        interval = 0
        last_call = time()
        time_ahead = Interval.interval_time_ahead(interval, last_call)
        self.assertEqual(time_ahead, 0)


class IntervalTestCase(unittest.TestCase):

    def setUp(self):
        self.interval = Interval()

    def test_init(self):
        self.interval.tick()
        self.assertEqual(self.interval.time_ahead(), 0)

    def test_set(self):
        self.interval.set(0)
        self.interval.tick()
        self.assertEqual(self.interval.time_ahead(), 0)
        self.interval.set(3)
        self.interval.tick()
        self.assertEqual(self.interval.time_ahead(), 3)

    def test_set_negative(self):
        with self.assertRaises(AssertionError) as cm:
            self.interval.set(-1)
        self.assertEqual(cm.exception.args[0], 'duration must be positive')


class TimeTestCase(unittest.TestCase):
       
    import time as time_test

    @staticmethod
    def mytime():
        return 3

    def setUp(self):
        self.time_test.time = self.mytime

    def test_time(self):
        interval = Interval()
        interval.set(5)
        interval.tick()
        with self.assertRaises(AssertionError) as cm:
            interval.time_ahead()
        self.assertEqual(cm.exception.args[0], 'last_tick must be in the past')
