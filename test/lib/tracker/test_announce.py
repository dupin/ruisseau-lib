import asyncio
from collections import namedtuple
import unittest

from ruisseau.lib.tracker.announce import Announcer, ResponseError

INFO_HASH = 20 * b'h'
PEER_ID = 20 * 'i'
PORT = 1337

INTERVAL_MIN = 60
INTERVAL_MAX = 3600

Response = namedtuple('Response', ['peers', 'interval'])


class RequestTestCase(unittest.IsolatedAsyncioTestCase):

    def setUp(self):
        self.adapter = HookAdapter()
        self.adapter.response = Response(None, 0)
        self.progress = {'uploaded': 0, 'downloaded': 0, 'left': 3}
        self.torrent_infos = {
            'info_hash': INFO_HASH,
            'peer_id': PEER_ID,
            'port': PORT,
            'progress': self.progress
        }
        self.tracker = Announcer(self.adapter, self.torrent_infos)

    def make_request(self, event=None):
        return {'info_hash': INFO_HASH,
                'peer_id': PEER_ID,
                'port': PORT,
                'uploaded': self.progress['uploaded'],
                'downloaded': self.progress['downloaded'],
                'left': self.progress['left'],
                'event': event
                }

    async def announce_and_assert_event(self, event, msg=None):
        expected = self.make_request(event)
        await self.tracker.announce()
        self.assertEqual(self.adapter.request, expected, msg)

    async def test_progress(self):
        await self.tracker.announce()
        self.assertEqual(self.adapter.request['uploaded'], 0)
        self.assertEqual(self.adapter.request['downloaded'], 0)
        self.assertEqual(self.adapter.request['left'], 3)

    async def test_event(self):
        """Check event during announcement sequence."""

        await self.announce_and_assert_event('started', 'first event must be started')
        await self.announce_and_assert_event(None, 'event must be None while regular announcement')
        self.progress['left'] = 0
        await self.announce_and_assert_event('completed', 'event must be completed when download complete')
        await self.announce_and_assert_event(None, 'event must be None while regular announcement')

        expected = self.make_request(event='stopped')
        await self.tracker.stop()
        self.assertEqual(self.adapter.request, expected, 'event must be stopped when tracker stop')
        await self.announce_and_assert_event('completed', 'started completed: event must be completed')

    async def test_event_with_failure(self):
        """Status must not change when failure"""
        valid_response = self.adapter.response
        self.adapter.response = Exception('fail reason')
        with self.assertRaises(Exception):
            await self.announce_and_assert_event('started', 'first event must be started')
        self.adapter.response = valid_response
        await self.announce_and_assert_event('started', 'last fail, must be started')


class ResponseTestCase(unittest.IsolatedAsyncioTestCase):

    def setUp(self):
        self.adapter = HookAdapter()
        self.progress = {'uploaded': 0, 'downloaded': 0, 'left': 3}
        self.torrent_infos = {
            'info_hash': INFO_HASH,
            'peer_id': PEER_ID,
            'port': PORT,
            'progress': self.progress
        }
        self.tracker = Announcer(self.adapter, self.torrent_infos)

    async def test_failure(self):
        self.adapter.response = Exception('test failure!')
        with self.assertRaises(Exception, msg='failure response must raise error') as cm:
            await self.tracker.announce()
        self.assertEqual(cm.exception.args[0], 'test failure!', 'failure reason mismatch')

    async def test_peers(self):
        expected_peers = ('p0', 'p1', 'p2')
        self.adapter.response = Response(expected_peers, 0)
        actual_peers = await self.tracker.announce()
        self.assertEqual(actual_peers, expected_peers)

    def test_interval_stopped(self):
        self.assertEqual(self.tracker.time_to_announce, 0)

    async def test_stop(self):
        self.adapter.response = Response(set(), INTERVAL_MIN + 1)
        await self.tracker.announce()
        self.adapter.response = Response(set(), INTERVAL_MIN + 2)
        await self.tracker.stop()
        self.assertEqual(self.tracker.time_to_announce, INTERVAL_MIN + 2)
        self.assertEqual(self.tracker.status, 'stopped')

    async def test_stop_failure(self):
        self.adapter.response = Response(set(), INTERVAL_MIN + 1)
        await self.tracker.announce()
        self.adapter.response = Exception('failure!')
        with self.assertRaises(Exception):
            await self.tracker.stop()

    async def test_interval_min(self):
        self.adapter.response = Response(set(), INTERVAL_MIN - 1)
        await self.tracker.announce()
        self.assertEqual(self.tracker.time_to_announce, INTERVAL_MIN)

    async def test_interval_max(self):
        self.adapter.response = Response(set(), INTERVAL_MAX + 1)
        await self.tracker.announce()
        self.assertEqual(self.tracker.time_to_announce, INTERVAL_MAX)

    async def test_interval_regular(self):
        self.adapter.response = Response(set(), INTERVAL_MIN + 1)
        await self.tracker.announce()
        self.assertEqual(self.tracker.time_to_announce, INTERVAL_MIN + 1)

    async def test_interval_invalid(self):
        self.adapter.response = Response(set(), 'invalid interval')
        with self.assertRaises(ResponseError, msg='invalid interval must raise error') as cm:
            await self.tracker.announce()
        self.assertEqual(cm.exception.response, self.adapter.response)
        self.assertEqual(self.tracker.time_to_announce, INTERVAL_MAX)

    async def test_interval_failure(self):
        self.adapter.response = Exception('failure!')
        with self.assertRaises(Exception):
            await self.tracker.announce()
        self.assertEqual(self.tracker.time_to_announce, INTERVAL_MAX)

    async def test_invalid_response(self):
        self.adapter.response = dict()
        with self.assertRaises(ResponseError, msg='invalid response must raise error') as cm:
            await self.tracker.announce()
        self.assertEqual(cm.exception.response, self.adapter.response)
        self.assertEqual(self.tracker.time_to_announce, INTERVAL_MAX)


class ConcurencyTestCase(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.adapter = DelayAdapter()
        self.progress = {'uploaded': 0, 'downloaded': 0, 'left': 3}
        self.torrent_infos = {
            'info_hash': INFO_HASH,
            'peer_id': PEER_ID,
            'port': PORT,
            'progress': self.progress
        }
        self.tracker = Announcer(self.adapter, self.torrent_infos)
        self.adapter.response = Response(set(), INTERVAL_MIN + 1)

    async def test_concurrent_announce(self):
        await asyncio.gather(
            self.tracker.announce(),
            self.tracker.announce()
        )
        self.assertIn(self.adapter.log, [[0, 0, 1, 1], [1, 1, 0, 0]])


class HookAdapter:

    def __init__(self):
        self.response = None

    async def announce(self, request):
        self.request = request
        if isinstance(self.response, Exception):
            raise self.response
        return self.response


class DelayAdapter(HookAdapter):

    def __init__(self):
        super().__init__()
        self.count = 0
        self.log = []

    async def announce(self, request):
        index = self.count
        self.count += 1
        self.log.append(index)
        await asyncio.sleep(0.001)
        self.log.append(index)
        return await super().announce(request)
