import asyncio
import unittest

from urllib import parse

from ruisseau.lib.tracker.adapter_factory import AdapterFactory
from ruisseau.lib.tracker.adapters.http import HttpAdapter, _UrlFactory, _ResponseFactory, AnnounceError


def make_request(*args):
    keys = ['info_hash', 'peer_id', 'port', 'uploaded', 'downloaded', 'left', 'event']
    tpl = zip(keys, args)
    return dict(tpl)


class LoadAdapterTestCase(unittest.TestCase):

    def test_load_adapter(self):
        factory = AdapterFactory()
        for scheme in ('http', 'https'):
            uri = scheme + '://somepath'
            adapter = factory.from_uri(uri)
            self.assertIs(adapter, HttpAdapter)


class AdapterTestCase(unittest.IsolatedAsyncioTestCase):
    IP = '127.0.0.1'
    PORT = 1337

    async def asyncSetUp(self):
        self.server = await asyncio.start_server(self.handle_client, self.IP, self.PORT)
        await self.server.start_serving()
        self.adapter = HttpAdapter(f'http://{self.IP}:{self.PORT}')

    def tearDown(self):
        self.server.close()

    async def handle_client(self, reader, writer):
        self.request = await reader.readline()
        writer.write(self.response)
        await writer.drain()
        writer.close()

    async def test_regular(self):
        self.response = b'HTTP/1.1 200 OK\r\n\r\nd8:intervali900e5:peers12:3\x9e\xb7\xed\xf4\xe6C\xdb\x88\x03\xc8\xd5e'
        req = make_request(b'i'*20, 'p'*20, 1, 2, 3, 4, 'started')
        resp = await self.adapter.announce(req)
        expected_request = b'GET /?info_hash=iiiiiiiiiiiiiiiiiiii&peer_id=pppppppppppppppppppp&port=1&uploaded=2&downloaded=3&left=4&event=started&compact=1 HTTP/1.1\r\n'
        self.assertEqual(self.request, expected_request)
        peers = [
            {"host": "51.158.183.237", "port": 62694},
            {"host": "67.219.136.3", "port": 51413}
        ]
        self.assertEqual(resp, {'peers': peers, 'interval': 900})

    async def test_404(self):
        self.response = b'HTTP/1.1 404 Not Found\r\n\r\n'
        req = make_request(b'i'*20, 'p'*20, 1, 2, 3, 4, 'started')
        with self.assertRaises(AnnounceError) as cm:
            await self.adapter.announce(req)
        self.assertEqual(cm.exception.args[0], 'HTTP 404')


class UrlTestCase(unittest.TestCase):

    def setUp(self):
        self.host = 'https://hostname.test/announce'
        self.request = make_request('info_hash', 'peer_id', 1337, 1, 2, 3, 'started')

    def test_url(self):
        url = _UrlFactory(self.host).from_request(self.request)
        self.assertTrue(url.startswith(self.host))
        expected_query = {'info_hash': ['info_hash'],
            'peer_id': ['peer_id'],
            'port': ['1337'],
            'uploaded': ['1'],
            'downloaded': ['2'],
            'left': ['3'],
            'event': ['started'],
            'compact': ['1']}
        self.assertEqual(expected_query, parse.parse_qs(parse.urlsplit(url).query))

    def test_not_compact(self):
        url = _UrlFactory(self.host, compact=False).from_request(self.request)
        result_query = parse.parse_qs(parse.urlsplit(url).query)
        self.assertEqual(['0'], result_query['compact'])


class ResponseTestCase(unittest.TestCase):

    def test_bin_peers(self):
        bencoded = b'd8:intervali900e5:peers12:3\x9e\xb7\xed\xf4\xe6C\xdb\x88\x03\xc8\xd5e'
        response = _ResponseFactory().from_bencoded(bencoded)
        peers = [
            {"host": "51.158.183.237", "port": 62694},
            {"host": "67.219.136.3", "port": 51413}
        ]
        expected = {'peers': peers, 'interval': 900}
        self.assertEqual(expected, response)

    def test_invalid_bin_peers(self):
        bencoded = b'd8:intervali900e5:peers11:3\x9e\xb7\xed\xf4\xe6C\xdb\x88\x03\xc8e'
        with self.assertRaises(ValueError) as cm:
            _ResponseFactory().from_bencoded(bencoded)
        self.assertEqual(cm.exception.args[0], 'Binary peers not multiple of 6 bytes')

    def test_dict_peers(self):
        bencoded = b'd8:intervali900e5:peersld2:ip12:123.1.23.1237:peer id20:AZERTYUIOPQSDFGHJKLM4:porti1337eeee'
        response = _ResponseFactory().from_bencoded(bencoded)
        peers = [{"host": "123.1.23.123", "port": 1337, "id": "AZERTYUIOPQSDFGHJKLM"}]
        expected = {'peers': peers, 'interval': 900}
        self.assertEqual(expected, response)

    def test_wrong_type_peers(self):
        bencoded = b'd8:intervali900e5:peersi123ee'
        with self.assertRaises(ValueError):
            _ResponseFactory().from_bencoded(bencoded)

    def test_failure_response(self):
        bencoded = b'd14:failure reason14:reason messagee'
        with self.assertRaises(AnnounceError) as cm:
            _ResponseFactory().from_bencoded(bencoded)
        self.assertEqual('reason message', cm.exception.args[0])
